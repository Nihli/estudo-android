/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import bean.Usuario;

/**
 *
 * @author Rafael
 */
public class UsuarioDAO {
    
    Connection con = new ConnectionFactory().getConnection();
    
    public Usuario autenticarLogin(String login){
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            ps = con.prepareStatement("SELECT LOGIN, SENHA FROM USUARIO WHERE LOGIN = ?");
            ps.setString(1, login);
            rs = ps.executeQuery();
            List<Usuario> list = new ArrayList<>();
            while (rs.next()) {
                Usuario u = new Usuario();
                u.setLogin(rs.getString("LOGIN"));
                u.setSenha(rs.getString("SENHA"));
                list.add(u);
            }
            if(!list.isEmpty())
                return list.get(0);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
