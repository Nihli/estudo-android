package com.example.appandroid;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthenticationActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener {

    public static final String REQUEST_TAG = "Mutantes";
    private EditText mLogin;
    private EditText mSenha;
    private Button mButton;
    private RequestQueue mQueue;
    private String url;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        mLogin = findViewById(R.id.editLogin);
        mSenha = findViewById(R.id.editSenha);
        mButton = findViewById(R.id.button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("ERRO");
        alert = builder.create();
    }

    @Override
    protected void onStart(){
        super.onStart();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeRequest();
            }
        });
    }

    public void makeRequest(){
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext()).
                getRequestQueue();
        url = "http://10.0.2.2:39473/Webservice/AutenticadorUsuario?login="+
                mLogin.getText().toString()+"&senha="+mSenha.getText().toString();
        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method.POST,
                url,new JSONObject(), this, this);
        jsonRequest.setTag(REQUEST_TAG);
        mQueue.add(jsonRequest);
    }

    @Override
    protected void onStop(){
        super.onStop();
        if(mQueue != null){
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {
        String res = null;
        try {
            res = ((JSONObject)response).getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(res.equals("true")){
            Intent intent = new Intent(AuthenticationActivity.this,
                    DashboardActivity.class);
            startActivity(intent);
            finish();
        }
        else{
            alert.setMessage("Login ou Senha incorretos!");
            alert.show();
        }
    }
}